package tk.shanebee.hg.game;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import tk.shanebee.hg.HG;
import tk.shanebee.hg.data.PlayerData;
import tk.shanebee.hg.util.ChatUtils;
import tk.shanebee.hg.util.Util;

/**
 * General team handler
 */
public class Team {

	private UUID leader;
	private List<UUID> players = new ArrayList<>();
	private List<UUID> pending = new ArrayList<>();
	
	public Team(UUID leader) {
		this.leader = leader;
		players.add(leader);
	}

    /** Invite a player to this team
     * <p>This will send the player a message inviting them to the team</p>
     * @param player Player to invite
     */
	public void invite(Player player) {
	    Player p = Bukkit.getPlayer(leader);
	    assert p != null;
		Util.scm(player, HG.getPlugin().getLang().team_invite_1);
		Util.scm(player, HG.getPlugin().getLang().team_invite_2.replace("<inviter>",p.getName()));
		TextComponent mainMessagePart1 = ChatUtils.createColoredMessage(HG.getPlugin().getLang().team_invite_3);
		TextComponent mainMessagePart2 = ChatUtils.createColoredMessage(HG.getPlugin().getLang().team_invite_5);
		TextComponent mainMessagePart3 = ChatUtils.createColoredMessage(HG.getPlugin().getLang().team_invite_7);
		TextComponent accept = ChatUtils.createClickableText(HG.getPlugin().getLang().team_invite_4,  HG.getPlugin().getLang().team_invite_hover_accept, "/hg team accept");
		TextComponent deny = ChatUtils.createClickableText(HG.getPlugin().getLang().team_invite_6,  HG.getPlugin().getLang().team_invite_hover_deny, "/hg team deny");
		TextComponent mainMessage = new TextComponent();
		mainMessage.addExtra(mainMessagePart1);
		mainMessage.addExtra(accept);
		mainMessage.addExtra(mainMessagePart2);
		mainMessage.addExtra(deny);
		mainMessage.addExtra(mainMessagePart3);
		player.spigot().sendMessage(mainMessage);
		Util.scm(player, HG.getPlugin().getLang().team_invite_8);
		pending.add(player.getUniqueId());
		PlayerData pd = HG.getPlugin().getPlayers().get(player.getUniqueId());
		pd.setTeam(this);
	}

    /** Accept the invite to this team
     * @param player Player to force to accept the invite
     */
	public void acceptInvite(Player player) {
		pending.remove(player.getUniqueId());
		players.add(player.getUniqueId());
		Util.scm(player, HG.getPlugin().getLang().joined_team);
	}

    /** Check if a player is on this team
     * @param uuid UUID of player to check
     * @return True if player is on this team
     */
	public boolean isOnTeam(UUID uuid) {
		return (players.contains(uuid));
	}

    /** Check if a player is pending an invite for this team
     * @param uuid UUID of player to check
     * @return True if this player is currently pending an invite for this team.
     */
	public boolean isPending(UUID uuid) {
		return (pending.contains(uuid));
	}

    /** Get the players on this team
     * @return List of UUIDs of players on this team
     */
	public List<UUID> getPlayers() {
		return players;
	}

    /** Get the pending players on this team
     * @return List of UUIDs of players pending to be on this team
     */
	public List<UUID> getPenders() {
		return pending;
	}

    /** Get the leader of this team
     * @return UUID of player who is leading this team
     */
	public UUID getLeader() {
		return leader;
	}
}
