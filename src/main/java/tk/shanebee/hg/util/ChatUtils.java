package tk.shanebee.hg.util;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class ChatUtils {

    public static TextComponent createColoredMessage(String messageText) {

        TextComponent coloredMessage = new TextComponent();
        coloredMessage.addExtra(ChatColor.translateAlternateColorCodes('&', messageText));

        return coloredMessage;
    }


    public static TextComponent createClickableText(String clickableMessageText, String hoverText, String commandOnClick) {

        //Create Base Message
        TextComponent clickableMessage = new TextComponent();
        clickableMessage.addExtra(ChatColor.translateAlternateColorCodes('&', clickableMessageText));

        //Create TextComponent that is shown in the hover event
        TextComponent hoverMessage = new TextComponent();
        hoverMessage.addExtra(ChatColor.translateAlternateColorCodes('&', hoverText));


        //Bind both together
        clickableMessage.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(hoverMessage).create()));
        clickableMessage.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, (commandOnClick)));
        return clickableMessage;
    }
}
